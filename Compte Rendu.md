Procédure de création des règles:
--
__(on Windows 7pro)__

>
>Conseil: Pour ce TP il est nécessaire de télécharger Thinderbird sur votre VM avant de commencer
>

<u>Vérifier les ports actifs et leur utilisation</u>

```bash
    C:/> netstat 
``` 

<i>
Cette commande permet de visualiser les ports utilisé sur votre machine <b>(CF Image 0)</b>. <br/>

- La Première colonne affiche le protocole utilisé sur ce port (TCP ou UDP)
- La seconde montre l'adresse accompagnée du port du processus locaux (trafic entrant)
- La troisième indique l'adresse distante qui interagis (trafic sortant)
- La dernière colonne indique l'état du processus
</i>

---

__1/ Bloquer tout les ports n'ayant pas de règles__

- Ouverture du panneau de configuration:
	- Menu Démarrer
	- Barre de recherche -> "Panneau de configuration"
- Système et sécurité
- Pare-feu Windows
- Dans le panneau lattéral -> Paramétres avancés _(CF Image 1)_
- Dans la nouvelle fenetre, vérifier dans le menu lattéral qu'on est bien dans "Pare-feu Windows avec fonctions avancés...."
	- Dans le paneau Central -> cliquer sur "Propriété du pare-feu windows" 
	- Dans les trois onglet de profil de la nouvelle fenêtre: _(CF Image 2)_
		- Selectionner "Tout bloquer" pour les Connexions entrantes
		- Selectionner "Bloquer" pour toutes les connexions sortantes.
		- Boutons "Appliquer" puis "Ok"

>
>Conseil: A partir de ce moment là, il est fortement conseilé de faire un clone ou une sauvegarde de votre VM
>

-----
	
__2/ Supprimer toute les règles__

- Dans le Panneau lattéral :
	- Règle de trafc entrant:
		- Selectionner toute les règle avec un `Ctrl+A` puis 
		- Les supprimer avec "suppr" de votre clavier ou "Supprimer" dans le panneau lattéral de droite	
	- Règle de trafic sortant
		- Selectionner toute les règle avec un `Ctrl+A`
		- Les supprimer avec "suppr" de votre clavier ou "Supprimer" dans le panneau lattéral de droite 

>
> Conseil : Vous pourrez vérifier en ouvrant votre navigateur ou a travers un "netstat" dans votre cmd, plus rien ne passe
>

-----

__3/ Ajouter des nouvelles règles de trafic Entrant ou Sortant__

- "Nouvelle Règle..." dans le panneau lattéral de droite
- "Port" -> Suivant _(CF Image 3)_
- le protocole auquel vous souhaitez appliquer la règle (tcp ou UDP) -> Le ou les port auxquels vous voulez appliquer la règle -> suivant _(CF Image 4)_
- Ce que vous voulez faire, tout est dans des propositions -> suivant _(CF Image 5)_
- Les profils pour lequels vous voulez appliquer votre règle, par défaut on laisse tout coché -> suivant _(CF Image 6)_
- Donnez un nom à votre nouvelle règle -> Terminer

-----
		

__4/ Installer Thunderbird et le configurer__

- Si vous avez déjà installé et configuré un mail, vous pouvez en ajouter un nouveau en cliquant sur "Courrier éléctonique" dans Compte -> Configurer un compte _(CF Image 7)_
- Indiquez votre nom -> Indiquez votre Adresse mail ("epsi_b3@thitux.com" pour notre cas) -> entrer le mot de passe (donné sur le sujet)
- Cliquer sur suivant puis immédiatement cliquer sur "Configuration Manuelle"
- Dans Serveur entrant:
```
	- "IMAP"
	- Nom d'hôte du serveur : "imap.1and1.fr"
	- Port : 993
	- SSL : autodetection (vérifier que ce soit bien "TLS")
	- Authentification : "epsi_b3@thitux.com"
``` 
- Dans serveur sortant:
```
    - Nom d'hôte du serveur : "auth.smtp.1and1.fr"
    - Port : 587
    - SSL : autodetection (vérifier que ce soit bien "TLS")
    - Authentification : "epsi_b3@thitux.com"
    - Identifiant : "epsi_b3@thitux.com"
    - Serveur Sortant : "epsi_b3@thitux.com"
```

_(CF Image 8)_
<br/>

>
> Conseil: Tester en s'envoyant un mail à soi même sur une adresse personnelle.
> 

<br/>

-----

__5/ Récupérer le flag:__

- aller sur internet et taper l'adresse de l'hote fournie précèdée de "ftp://"  _(CF Image 9)_
- Entrer les identifiant lors de l'ouverture de la fenêtre contextuelle d'identification _(CF Image 10)_
- Récupérer le Flag _(CF Image 11)_
